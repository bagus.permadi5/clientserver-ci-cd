# Server Client Apps
## How to start

To Create CI/CD build java app Client-Server


Step by Step


1. We Need to Create Webhook connection to jenkins


![](images/webhook.jpg)




2. In Jenkins Gui, We Create Freestyle Job Build and add SCM config for pulling code from gitlab





![](images/scm.jpg)




3. In Jenkins Gui, We also Create Build trigger to validate gitlab webhook connection





![](images/trigger.jpg)





4. In Jenkins Gui, We also have to Create post Build code to server and define command to deploy the app.

Picture below is to build server service




![](images/BuildServer.jpg)



Picture below is to build client service



![](images/BuildCLient.jpg)





Client Ouput

![](images/client.jpg)




Server Output

![](images/server.jpg)
